# Note
```javascript
 columnHintRender (elem, self, data) {
    const divName = document.createElement("div");
    divName.className = "autocomplete-column-name";
    divName.innerText = data.columnName;

    const divHint = document.createElement("div");
    divHint.className = "autocomplete-column-hint";
    divHint.innerText = data.columnHint;
    elem.append(divName, divHint);
  }

codemirror table hints
tables: [
 {
  text: 'users',
  columns: [
   { text: 'gender', displayText: 'gender\tvarchar(200)', columnName: 'gender', columnHint: 'varchar(200)',  render: columnHintRender }
  ]
 }
],
defaultTable: 'users'
```

